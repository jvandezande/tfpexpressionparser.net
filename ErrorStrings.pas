﻿{
    This file was part of the Free Component Library (FCL)
    Copyright (c) 2008 Michael Van Canneyt.

    Modified for Oxygene by Jeroen Vandezande Copyright (c) 2016

    Expression parser, supports variables, functions and
    float/string/boolean operations.

    The source code of the Free Pascal Runtime Libraries and packages are 
    distributed under the Library GNU General Public License 
    with the following modification:

    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the terms
    and conditions of the license of that module. An independent module is a module
    which is not derived from or based on this library. If you modify this
    library, you may extend this exception to your version of the library, but you are
    not obligated to do so. If you do not wish to do so, delete this exception
    statement from your version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 }
namespace fpexprpars;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Text;

type
  ErrorStrings = public static class
  assembly
    SBadQuotes        := 'Unterminated string';
    SUnknownDelimiter := 'Unknown delimiter character: "{0}"';
    SErrUnknownCharacter := 'Unknown character at pos {0}: "{0}"';
    SErrUnexpectedEndOfExpression := 'Unexpected end of expression';
    SErrUnknownComparison := 'Internal error: Unknown comparison';
    SErrUnknownBooleanOp := 'Internal error: Unknown boolean operation';
    SErrBracketExpected := 'Expected ) bracket at position {0}, but got {1}';
    SerrUnknownTokenAtPos := 'Unknown token at pos {0} : {1}';
    SErrLeftBracketExpected := 'Expected ( bracket at position {0}, but got {1}';
    SErrInvalidFloat := '{0} is not a valid floating-point value';
    SErrUnknownIdentifier := 'Unknown identifier: {0}';
    SErrInExpression := 'Cannot evaluate: error in expression';
    SErrInExpressionEmpty := 'Cannot evaluate: empty expression';
    SErrCommaExpected :=  'Expected comma (,) at position {0}, but got {1}';
    SErrInvalidNumberChar := 'Unexpected character in number : {0}';
    SErrInvalidNumber := 'Invalid numerical value : {0}';
    SErrNoOperand := 'No operand for unary operation {0}';
    SErrNoleftOperand := 'No left operand for binary operation {0}';
    SErrNoRightOperand := 'No right operand for binary operation {0}';
    SErrNoNegation := 'Cannot negate expression of type {0} : {1}';
    SErrNoNOTOperation := 'Cannot perform "not" on expression of type {0}: {1}';
    SErrTypesDoNotMatch := 'Type mismatch: {0}<>{1} for expressions "{2}" and "{3}".';   
    SErrNoNodeToCheck := 'Internal error: No node to check !';
    SInvalidNodeType := 'Node type ({0}) not in allowed types ({1}) for expression: {2}';
    SErrUnterminatedExpression := 'Badly terminated expression. Found token at position {0} : {1}';
    SErrDuplicateIdentifier := 'An identifier with name "%s" already exists.';
    SErrInvalidResultCharacter := '"{0}" is not a valid return type indicator';
    ErrInvalidArgumentCount := 'Invalid argument count for method {0}';
    SErrInvalidArgumentType := 'Invalid type for argument {0}: Expected {1}, got {2}';
    SErrInvalidResultType := 'Invalid aResult type: {0}';
    SErrNotVariable := 'Identifier {0} is not a variable';  
    SErrIFNeedsBoolean := 'First argument to IF must be of type boolean: {0}';
    SErrCaseNeeds3 := 'Case statement needs to have at least 4 arguments';
    SErrCaseEvenCount := 'Case statement needs to have an even number of arguments';
    SErrCaseLabelNotAConst := 'Case label {0} "{1}" is not a constant expression';
    SErrCaseLabelType := 'Case label {0} "{1}" needs type {2}, but has type {3}';
    SErrCaseValueType := 'Case value {0} "{1}" needs type {2}, but has type {3}';
  end;

implementation

end.
