﻿{
    This file was part of the Free Component Library (FCL)
    Copyright (c) 2008 Michael Van Canneyt.

    Modified for Oxygene by Jeroen Vandezande Copyright (c) 2016

    Expression parser, supports variables, functions and
    float/string/boolean operations.

    The source code of the Free Pascal Runtime Libraries and packages are
    distributed under the Library GNU General Public License
    with the following modification:

    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the terms
    and conditions of the license of that module. An independent module is a module
    which is not derived from or based on this library. If you modify this
    library, you may extend this exception to your version of the library, but you are
    not obligated to do so. If you do not wish to do so, delete this exception
    statement from your version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 }
namespace fpexprpars;

interface

uses
  System.Collections.ObjectModel,
  System.Collections.Generic,
  System.Collections.Specialized,
  System.Collections.Specialized,
  System.Text,
  System.Linq;

Type

  ExpressionException = public Class(Exception);

  TTokenType = assembly enum(Plus, Minus, LessThan, LargerThan, Equal, &Div,
                              Mul, Power, Left, Right, LessThanEqual, LargerThanEqual,
                              Unequal, Number, String, Identifier,
                              Comma, &and, &Or,&Xor,&True,&False,&not,&if,
                              &Case,EOF);

  [System.Diagnostics.DebuggerNonUserCode]
  TFPExpressionScanner = public Class
  private
    fSource: String;
    FTokenType: TTokenType;
    method GetCurrentChar: Char;
  assembly
    fSourceLength, fSourcePosition: Integer;
    method ScanError(aMessage, aInvalidToken: String; aInvalidCharacterPosition: Integer);
    method SetSource(const AValue: String); virtual;
    method DoIdentifier: TTokenType;
    method DoNumber: TTokenType;
    method DoDelimiter: TTokenType;
    method DoString: TTokenType;
    method NextPos: Char;
    method SkipWhiteSpace;
    method IsWordDelim(c: Char): Boolean;
    method IsDelim(c: Char): Boolean;
    method IsDigit(c: Char): Boolean;
    method IsAlpha(c: Char): Boolean;
    method GetToken: TTokenType;
    Property TokenType: TTokenType Read FTokenType;
    const cDoubleQuote = '"';
    const cNull = #0;
    const Digits: array of Char        = ['0','1','2','3','4','5','6','7','8','9','.'];
    const WhiteSpace: array of Char    = [' ',#13,#10,#9];
    const Operators: array of Char     = ['+','-','<','>','=','/','*'];
    const Delimiters: array of Char    = ['+','-','<','>','=','/','*',',','(',')', '^'];
    const Symbols: array of Char       = ['%','^', '+','-','<','>','=','/','*',',','(',')'];
    const WordDelimiters: array of Char = [' ',#13,#10,#9,'%','^', '+','-','<','>','=','/','*',',','(',')'];
    const ttDelimiters = [TTokenType.Plus, TTokenType.Minus, TTokenType.LessThan, TTokenType.LargerThan, TTokenType.Equal, TTokenType.Div,
                          TTokenType.Mul, TTokenType.Left, TTokenType.Right, TTokenType.LessThanEqual, TTokenType.LargerThanEqual,
                          TTokenType.Unequal];
  public
    Constructor;
    Property Token: String;
    Property Source: String Read fSource Write SetSource;
    Property Pos: Integer Read fSourcePosition;
    Property CurrentChar: Char Read GetCurrentChar;
  end;

  ExpressionScannerException = public class(ExpressionException)
  private
    constructor;
  public
    property InvalidCharacterPosition: Integer;
    property InvalidToken: String;
    constructor(aMessage, aInvalidToken: String; aInvalidCharacterPosition: Integer);
  end;

  TResultType = public enum(Boolean, Float, String);

  TResultTypes = set of TResultType;

  TFPExpressionResult = public record
  public
    ResultType: TResultType;
    ResFloat: Double;
    ResBoolean: Boolean;
    ResString: String;
  end;

  TExprParameterArray = public array of TFPExpressionResult;

  { TFPExprNode }
  TFPExprNode = abstract public class
  private
  Protected
    method CheckNodeType(aNode: TFPExprNode; Allowed: TResultTypes);
  Public
    method GetNodeValue(var aResult: TFPExpressionResult); virtual; abstract;
    method Check; virtual; abstract;
    method NodeType: TResultType; virtual; abstract;
    method NodeValue: TFPExpressionResult;
    property UsedInBinaryOperation: Boolean;
    constructor; virtual;
  end;

  TExprArgumentArray = array of TFPExprNode;

  { TFPBinaryOperation }
  TFPBinaryOperation = abstract class(TFPExprNode)
  private
    FLeft: TFPExprNode;
    FRight: TFPExprNode;
  Protected
    method CheckSameNodeTypes;
  Public
    Constructor(ALeft,ARight : TFPExprNode); virtual;
    finalizer;
    method Check; override;
    Property Left : TFPExprNode Read FLeft;
    Property Right : TFPExprNode Read FRight;
  end;

  { TFPBooleanOperation }
  TFPBooleanOperation = abstract Class(TFPBinaryOperation)
  Public
    method Check; override;
    method NodeType : TResultType; override;
  end;

  { TFPBinaryAndOperation }
  TFPBinaryAndOperation = sealed class(TFPBooleanOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString: String; override;
  end;

  { TFPBinaryOrOperation }
  TFPBinaryOrOperation = sealed class(TFPBooleanOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPBinaryXOrOperation }
  TFPBinaryXOrOperation = sealed class(TFPBooleanOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPBooleanaResultOperation }
  TFPBooleanResultOperation = abstract Class(TFPBinaryOperation)
  Public
    method Check; override;
    method NodeType : TResultType; override;
  end;

  TFPBooleanResultOperationClass = Class of TFPBooleanResultOperation;

  { TFPEqualOperation }
  TFPEqualOperation = Class(TFPBooleanResultOperation)
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPUnequalOperation }
  TFPUnequalOperation = sealed class(TFPEqualOperation)
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPOrderingOperation }
  TFPOrderingOperation = abstract Class(TFPBooleanResultOperation)
  Public
    method Check; override;
  end;

  { TFPLessThanOperation }
  TFPLessThanOperation = Class(TFPOrderingOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPGreaterThanOperation }
  TFPGreaterThanOperation = Class(TFPOrderingOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPLessThanEqualOperation }
  TFPLessThanEqualOperation = sealed class(TFPGreaterThanOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPGreaterThanEqualOperation }
  TFPGreaterThanEqualOperation = sealed class(TFPLessThanOperation)
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TIfOperation }
  TIfOperation = sealed class(TFPBinaryOperation)
  private
    fCondition: TFPExprNode;
  protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method Check; override;
    method NodeType : TResultType; override;
    Constructor(ACondition,ALeft,ARight: TFPExprNode);
    finalizer;
    method ToString : String ; override;
    Property Condition : TFPExprNode Read fCondition;
  end;

  { TCaseOperation }
  TCaseOperation = sealed class(TFPExprNode)
  private
    fArgs: TExprArgumentArray;
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method Check; override;
    method NodeType: TResultType; override;
    Constructor(Args: TExprArgumentArray);
    finalizer;
    method ToString: String; override;
  end;

  { TMathOperation }
  TMathOperation = abstract Class(TFPBinaryOperation)
  Public
    method Check; override;
    method NodeType : TResultType; override;
  end;

  { TFPAddOperation }
  TFPAddOperation = sealed class(TMathOperation)
  private
  Protected
  Public
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPSubtractOperation }
  TFPSubtractOperation = sealed class(TMathOperation)
  Public
    method Check; override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
    method ToString : String ; override;
  end;

  { TFPMultiplyOperation }
  TFPMultiplyOperation = sealed class(TMathOperation)
  Public
    method Check; override;
    method ToString: String ; override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
  end;

  TFPPowerOperation = sealed class(TMathOperation)
  Public
    method Check; override;
    method ToString: String ; override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
  end;

  { TFPDivideOperation }
  TFPDivideOperation = sealed class(TMathOperation)
  Public
    method Check; override;
    method ToString : String ; override;
    method NodeType : TResultType; override;
    method GetNodeValue(var aResult : TFPExpressionResult); override;
  end;

  { TFPUnaryOperator }
  TFPUnaryOperator = abstract Class(TFPExprNode)
  private
    FOperand: TFPExprNode;
  Public
    Constructor(AOperand : TFPExprNode); virtual;
    finalizer;
    method Check; override;
    Property Operand: TFPExprNode Read FOperand;
  end;

  { TFPConvertNode }
  TFPConvertNode = abstract Class(TFPUnaryOperator)
  public
    method ToString : String; override;
  end;

  { TFPNotNode }
  TFPNotNode = Class(TFPUnaryOperator)
  Public
    method Check; override;
    method NodeType : TResultType; override;
    method GetNodeValue(var aResult : TFPExpressionResult); override;
    method ToString : String; override;
  end;

  { TFPNegateOperation }
  TFPNegateOperation = Class(TFPUnaryOperator)
  Public
    method Check; override;
    method NodeType : TResultType;  override;
    method GetNodeValue(var aResult : TFPExpressionResult);  override;
    method ToString : String; override;
  end;

  { TFPConstExpression }
  TFPConstExpression = Class(TFPExprNode)
  private
    FValue : TFPExpressionResult;
  public
    Constructor(AValue : String);
    Constructor(AValue : Int64);
    Constructor(AValue : Double);
    Constructor(AValue : Boolean);
    method Check; override;
    method NodeType: TResultType; override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
    method ToString: String; override;
    // For inspection
    Property ConstValue : TFPExpressionResult read FValue;
  end;

  TIdentifierType = public enum(Variable, &Function);

  TFPExprIdentifierDef = public class
  private
    method GetResultType: TResultType;
    method GetName: String;
    FStringValue: String;
    FName: String;
  assembly
  Protected
    method CheckResultType(Const AType: TResultType);
    method CheckVariable;
  Public
    property ArgumentCount: Integer;
    Property &Function: IFormulaFunction;
    Property IdentifierType: TIdentifierType;
    Property Name: String read GetName;
    Property ResultType: TResultType Read GetResultType;
    Property Variable: FormulaVariable;
  end;

  { TFPExprIdentifierDefs }
  TFPExprIdentifierDefs =  public Class(ObservableCollection<TFPExprIdentifierDef>)
  private
    method GetI(AIndex: Integer): TFPExprIdentifierDef;
    method SetI(AIndex: Integer; const AValue: TFPExprIdentifierDef);
  Protected
    Property Parser: TFPExpressionParser Read FParser;
  assembly
    FParser: TFPExpressionParser;
  Public
    method IndexOfIdentifier(Const AName : String): Integer;
    method FindIdentifier(Const AName : String): TFPExprIdentifierDef;
    method IdentifierByName(Const AName : String) : TFPExprIdentifierDef;
    method AddVariable(aVariable: FormulaVariable): TFPExprIdentifierDef;
    /// <summary>
    /// Recommended way to add multiple variables performance wise.
    /// </summary>
    /// <param name="aVariableRange"></param>
    /// <returns></returns>
    method AddVariableRange(aVariableRange: IEnumerable<FormulaVariable>): List<TFPExprIdentifierDef>;
    method AddFunction(aFunctionObject: IFormulaFunction): TFPExprIdentifierDef;
    /// <summary>
    /// Recommended way to add multiple functions performance wise.
    /// </summary>
    /// <param name="aFunctionObjectRange"></param>
    /// <returns></returns>
    method AddFunctionRange(aFunctionObjectRange: IEnumerable<IFormulaFunction>): List<TFPExprIdentifierDef>;
    property Identifier[AIndex: Integer]: TFPExprIdentifierDef Read GetI Write SetI; Default;
  end;

  { TFPExprIdentifierNode }
  TFPExprIdentifierNode = public abstract Class(TFPExprNode)
  Private
    FResultType : TResultType;
  protected
    fID : TFPExprIdentifierDef;
  public
    Constructor(aID: TFPExprIdentifierDef); 
    method NodeType: TResultType;  override;
    Property Identifier: TFPExprIdentifierDef Read fID;
  end;

  { TFPExprVariable }
  TFPExprVariable = public Class(TFPExprIdentifierNode)
  public
    method Check; override;
    method ToString: String; override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
  end;

  { TFPExprFunction }
  TFPExprFunction = Class(TFPExprIdentifierNode)
  private
    FArgumentNodes: TExprArgumentArray := new TFPExprNode[0];
  Protected
    FargumentParams: TExprParameterArray := new TFPExpressionResult[0];
    method CalcParams;
  Public
    method Check; override;
    constructor(AID: TFPExprIdentifierDef; Const Args: TExprArgumentArray); virtual;
    finalizer;
    Property ArgumentNodes: TExprArgumentArray Read FArgumentNodes;
    Property ArgumentParams: TExprParameterArray Read FargumentParams;
    method ToString : String; override;
  end;

  { TFPFunctionCallBack }
  TFPFunctionCallBack = Class(TFPExprFunction)
  Public
    Constructor(AID: TFPExprIdentifierDef; Const Args: TExprArgumentArray); override;
    method GetNodeValue(var aResult: TFPExpressionResult); override;
    Property CallBack: IFormulaFunction;
  end;

  TFPHashObjectlist = Dictionary<String, TFPExprIdentifierDef>;

  { TFPExpressionParser }
  [System.Diagnostics.DebuggerNonUserCode]
  TFPExpressionParser = public class
  private
    method setFunctions(value: TFPExprIdentifierDefs);
    fFunctions: TFPExprIdentifierDefs;
    method SetVariables(value: TFPExprIdentifierDefs);
    fVariables: TFPExprIdentifierDefs;
    fExpression: String;
    method SetExpression(const aValue: String); virtual;
    fScanner: TFPExpressionScanner;
    fExprNode: TFPExprNode;
    fHashList: TFPHashObjectlist;
    method CheckEOF;
    method GetAsBoolean: Boolean;
    method GetAsFloat: Double;
    method GetToString: String;
    method IdentifiersCollectionChanged(sender: Object; e: System.Collections.Specialized.NotifyCollectionChangedEventArgs);
  assembly
    method ParserError(Msg: String);
    method CheckResultType(Const Res: TFPExpressionResult; AType: TResultType);
    method Level1: TFPExprNode;
    method Level2: TFPExprNode;
    method Level3: TFPExprNode;
    method Level4: TFPExprNode;
    method Level5: TFPExprNode;
    method Level6: TFPExprNode;
    method Primitive: TFPExprNode;
    method GetToken: TTokenType;
    method TokenType: TTokenType;
    method CurrentToken: String;
    method CreateHashList;
    Property Scanner: TFPExpressionScanner Read fScanner;
    Property ExprNode: TFPExprNode Read fExprNode;
    const  ttComparisons = [TTokenType.LargerThan, TTokenType.LessThan, TTokenType.LargerThanEqual, TTokenType.LessThanEqual, TTokenType.Equal, TTokenType.Unequal];
  assembly
    fDirty: Boolean;
  public
    constructor;
    finalizer;
    method IdentifierByName(aName: String): TFPExprIdentifierDef; virtual;
    method Clear;
    method EvaluateExpression(Var aResult: TFPExpressionResult);
    method Evaluate: TFPExpressionResult;
    method ResultType: TResultType;
    method GetAllFunctions: List<IFormulaFunction>;
    Property AsFloat: Double Read GetAsFloat;
    Property ToString: String Read GetToString;
    Property AsBoolean: Boolean Read GetAsBoolean;
    property Expression: String read fExpression write SetExpression;
    Property Variables: TFPExprIdentifierDefs read fVariables write SetVariables;
    Property Functions: TFPExprIdentifierDefs read fFunctions write setFunctions;
    property UsedVariablesInFormula: List<TFPExprVariable> := new List<TFPExprVariable>;
  end;

  ExpressionParserException = public class(ExpressionException);

implementation

uses
  System.Collections.Specialized;

{ ---------------------------------------------------------------------
TFPExpressionScanner
---------------------------------------------------------------------}
method TFPExpressionScanner.IsAlpha(c: Char): Boolean;
begin
  Result := Char.IsLetter(c) or (c = '_') or (c = '@');
end;

constructor TFPExpressionScanner;
begin
  Source := String.Empty;
end;

method TFPExpressionScanner.SetSource(const AValue: String);
begin
  fSource := AValue;
  fSourceLength := fSource.Length;
  FTokenType := TTokenType.EOF;
  fSourcePosition := 0;
  Token := String.Empty;
end;

method TFPExpressionScanner.NextPos: Char;
begin
  inc(fSourcePosition);
  result := GetCurrentChar();
end;

method TFPExpressionScanner.IsWordDelim(c: Char): Boolean;
begin
  Result := WordDelimiters.Contains(c);
end;

method TFPExpressionScanner.IsDelim(c: Char): Boolean;
begin
  Result := Delimiters.Contains(c);
end;

method TFPExpressionScanner.IsDigit(c: Char): Boolean;
begin
  Result := Digits.Contains(c);
end;

method TFPExpressionScanner.SkipWhiteSpace;
begin
  while (fSourcePosition < fSourceLength) and Char.IsWhiteSpace(GetCurrentChar()) do NextPos();
end;

method TFPExpressionScanner.DoDelimiter: TTokenType;
begin
  var c := GetCurrentChar();
  Token := c;
  var b := c in ['<','>'];
  var d := c;
  c := NextPos();
  if b and (c in ['=','>']) then
  begin
    Token := Token + c ;
    NextPos();
    If (d = '>') then
      result := TTokenType.LargerThanEqual
    else if (c = '>') then
      result := TTokenType.Unequal
    else
      result := TTokenType.LessThanEqual;
  end
  else
  begin
    case d of
      '+' : Result := TTokenType.Plus;
      '-' : Result := TTokenType.Minus;
      '<' : Result := TTokenType.LessThan;
      '>' : Result := TTokenType.LargerThan;
      '=' : Result := TTokenType.Equal;
      '/' : Result := TTokenType.Div;
      '*' : Result := TTokenType.Mul;
      '^' : result := TTokenType.Power;
      '(' : Result := TTokenType.Left;
      ')' : Result := TTokenType.Right;
      ',' : Result := TTokenType.Comma;
      else
        ScanError(String.Format(ErrorStrings.SUnknownDelimiter, d), d, fSourcePosition);
    end;
  end;
end;

method TFPExpressionScanner.ScanError(aMessage, aInvalidToken: String; aInvalidCharacterPosition: Integer);
begin
  raise new ExpressionScannerException(aMessage, aInvalidToken, aInvalidCharacterPosition - aInvalidToken.Length)
end;

method TFPExpressionScanner.DoString : TTokenType;

  method TerminatingChar(c: Char): Boolean;
  begin
    Result := (c = cNull) or (c = cDoubleQuote);
  end;

begin
  Token := '';
  var c := CurrentChar;
  if (c <> cDoubleQuote) then ScanError(ErrorStrings.SBadQuotes, c, fSourcePosition);
  c := NextPos();
  while not TerminatingChar(c) do
  begin
    Token := Token + c;
    c := NextPos();
  end;
  if (c = cNull) then ScanError(ErrorStrings.SBadQuotes, c, fSourcePosition);
  Result := TTokenType.String;
  FTokenType := Result;
  NextPos();
end;

method TFPExpressionScanner.GetCurrentChar: Char;
begin
  if fSourcePosition < 0 then exit #0;
  result := iif(fSourcePosition < fSourceLength, fSource[fSourcePosition], #0)
end;

method TFPExpressionScanner.DoNumber: TTokenType;
var
  x: Double;
begin
  var c := CurrentChar;
  var prevC := #0;
  while (not IsWordDelim(c) or (prevC='E')) and (c <> cNull) do
  begin
    If Not ( IsDigit(c)
    or (not String.IsNullOrEmpty(Token) and (Char.ToUpper(c) = 'E'))
    or (not String.IsNullOrEmpty(Token) and (c in ['+','-']) and (prevC='E'))
    )
    then ScanError(String.Format(ErrorStrings.SErrInvalidNumberChar, c), c, fSourcePosition);
    Token := Token + c;
    prevC := Char.ToUpper(c);
    c := NextPos();
  end;
  if not Double.TryParse(Token, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out x) then ScanError(String.Format(ErrorStrings.SErrInvalidNumber, Token), Token, fSourcePosition);
  Result := TTokenType.Number;
end;

method TFPExpressionScanner.DoIdentifier: TTokenType;
begin
  var c := CurrentChar;
  while (not IsWordDelim(c)) and (c <> cNull) do
  begin
    Token := Token + c;
    c := NextPos();
  end;
  var s := Token.ToLower();
  If (s='or') then
    Result:= TTokenType.Or
  else if (s='xor') then
  Result:= TTokenType.Xor
    else if (s='and') then
  Result:= TTokenType.and
    else if (s='true') then
  Result:= TTokenType.True
      else if (s='false') then
  Result:= TTokenType.False
      else if (s='not') then
  Result:= TTokenType.not
        else if (s='if') then
  Result:= TTokenType.if
        else if (s='case') then
  Result:= TTokenType.Case
          else
  Result:= TTokenType.Identifier;
end;

method TFPExpressionScanner.GetToken: TTokenType;
begin
  Token := '';
  SkipWhiteSpace();
  var c := GetCurrentChar();
  if c = cNull then
    Result := TTokenType.EOF
  else if IsDelim(c) then
    Result := DoDelimiter()
  else if (c = cDoubleQuote) then
    Result := DoString()
  else if IsDigit(c) then
    Result := DoNumber()
  else if IsAlpha(c) then
    Result := DoIdentifier()
  else
    ScanError(String.Format(ErrorStrings.SErrUnknownCharacter, fSourcePosition, c), c, fSourcePosition);
  FTokenType := Result;
end;

{ ---------------------------------------------------------------------
TFPExpressionParser
---------------------------------------------------------------------}
method TFPExpressionParser.TokenType : TTokenType;
begin
  Result := fScanner.TokenType;
end;

method TFPExpressionParser.CurrentToken: String;
begin
  Result := fScanner.Token;
end;

method TFPExpressionParser.CreateHashList;
begin
  fHashList.Clear();
  // functions
  for each &function in Functions do
  begin
    fHashList.Add(&function.Name.ToLower(), &function);
  end;
  // variables
  for each variable in Variables do
  begin
    fHashList.Add(variable.Name.ToLower(), variable);
  end;
  fDirty := false;
end;

method TFPExpressionParser.IdentifierByName(aName: String): TFPExprIdentifierDef;
begin
  if fDirty then CreateHashList();
  var name := aName.ToLower();
  if not fHashList.ContainsKey(name) then Scanner.ScanError(String.Format(ErrorStrings.SErrUnknownIdentifier, name), name, Scanner.fSourcePosition);
  result := fHashList[name];
end;

method TFPExpressionParser.Clear;
begin
  fExpression := String.Empty;
  fDirty := true;
  fHashList.Clear;
  UsedVariablesInFormula.Clear();
  disposeAndNil(fExprNode);
end;

constructor TFPExpressionParser;
begin
  Variables := new TFPExprIdentifierDefs;
  Variables.FParser := self;
  Functions := new TFPExprIdentifierDefs;
  Functions.FParser := self;
  fScanner := new TFPExpressionScanner;
  fHashList := new TFPHashObjectlist;
end;

finalizer TFPExpressionParser;
begin
  disposeAndNil(fHashList);
  disposeAndNil(fExprNode);
  disposeAndNil(fScanner);
end;

method TFPExpressionParser.GetToken : TTokenType;
begin
  Result := fScanner.GetToken();
end;

method TFPExpressionParser.CheckEOF;
begin
  If (TokenType = TTokenType.EOF) then ParserError(ErrorStrings.SErrUnexpectedEndOfExpression);
end;

method TFPExpressionParser.EvaluateExpression(var aResult: TFPExpressionResult);
begin
  If String.IsNullOrEmpty(fExpression) then ParserError(ErrorStrings.SErrInExpressionEmpty);
  if not assigned(fExprNode) then ParserError(ErrorStrings.SErrInExpression);
  fExprNode.GetNodeValue(var aResult);
end;

method TFPExpressionParser.ParserError(Msg: String);
begin
  raise new ExpressionParserException(Msg);
end;

method TFPExpressionParser.GetAsBoolean: Boolean;
var
  res: TFPExpressionResult;
begin
  EvaluateExpression(var res);
  CheckResultType(res, TResultType.Boolean);
  Result := res.ResBoolean;
end;

method TFPExpressionParser.GetAsFloat: Double;
var
  Res: TFPExpressionResult;
begin
  EvaluateExpression(var Res);
  CheckResultType(Res, TResultType.Float);
  Result := Res.ResFloat;
end;

method TFPExpressionParser.GetToString: String;
var
  Res: TFPExpressionResult;
begin
  EvaluateExpression(var Res);
  CheckResultType(Res, TResultType.String);
  Result := Res.ResString;
end;

method TFPExpressionParser.Level1 : TFPExprNode;
var
  tt: TTokenType;
  Right: TFPExprNode;
begin
  {$ifdef debugexpr}Writeln('Level 1 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  if TokenType = TTokenType.not then
  begin
    GetToken();
    CheckEOF;
    Right := Level2();
    Result := new TFPNotNode(Right);
  end
  else
    Result := Level2();
    Try
      while (TokenType in [TTokenType.and, TTokenType.Or, TTokenType.Xor]) do
      begin
        tt := TokenType;
        GetToken;
        CheckEOF;
        Right := Level2();
        Right.Check();
        Case tt of
          TTokenType.Or  : Result := new TFPBinaryOrOperation(Result, Right);
          TTokenType.and : Result := new TFPBinaryAndOperation(Result, Right);
          TTokenType.Xor : Result := new TFPBinaryXOrOperation(Result, Right);
          Else
            ParserError(ErrorStrings.SErrUnknownBooleanOp)
        end;
      end;
    Except
      disposeAndNil(Result);
        Raise;
      end;
end;

method TFPExpressionParser.Level2: TFPExprNode;
var
  Right : TFPExprNode;
  tt : TTokenType;
begin
  {$ifdef debugexpr}  Writeln('Level 2 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  Result := Level3();
  try
    if ttComparisons.Contains(TokenType) then
    begin
      tt := TokenType;
      GetToken;
      CheckEOF;
      Right := Level3();
      Right.Check();
      Case tt of
        TTokenType.LessThan         : result := new TFPLessThanOperation(Result, Right);
        TTokenType.LessThanEqual    : result := new TFPLessThanEqualOperation(Result, Right);
        TTokenType.LargerThan       : result := new TFPGreaterThanOperation(Result, Right);
        TTokenType.LargerThanEqual  : result := new TFPGreaterThanEqualOperation(Result, Right);
        TTokenType.Equal            : result := new TFPEqualOperation(Result, Right);
        TTokenType.Unequal          : result := new TFPUnequalOperation(Result, Right);
      Else
        ParserError(ErrorStrings.SErrUnknownComparison)
      end;
    end;
  Except
    disposeAndNil(Result);
      Raise;
    end;
end;

method TFPExpressionParser.Level3: TFPExprNode;
var
  tt : TTokenType;
  right : TFPExprNode;
begin
  {$ifdef debugexpr}  Writeln('Level 3 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  Result := Level4();
  try
    while [TTokenType.Plus,TTokenType.Minus].Contains(TokenType) do
    begin
      tt := TokenType;
      GetToken;
      CheckEOF;
      right := Level4();
      right.Check();
      Case tt of
        TTokenType.Plus  : Result := new TFPAddOperation(Result, right);
        TTokenType.Minus : Result := new TFPSubtractOperation(Result, right);
      end;
    end;
  Except
    disposeAndNil(Result);
      Raise;
    end;
end;




method TFPExpressionParser.Level4: TFPExprNode;
var
  tt : TTokenType;
  right : TFPExprNode;
begin
  {$ifdef debugexpr}  Writeln('Level 4 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  Result := Level5();
  try
    while (TokenType in [TTokenType.Mul,TTokenType.Div, TTokenType.Power]) do
    begin
      tt := TokenType;
      GetToken;
      right := Level5();
      right.Check();
      Case tt of
        TTokenType.Mul   : Result := new TFPMultiplyOperation(Result, right);
        TTokenType.Div   : Result := new TFPDivideOperation(Result, right);
        TTokenType.Power : Result := new TFPPowerOperation(Result, right);
      end;
    end;
  Except
    disposeAndNil(Result);
    raise;
  end;
end;

method TFPExpressionParser.Level5: TFPExprNode;
begin
  {$ifdef debugexpr}  Writeln('Level 5 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  var b := false;
  if (TokenType in [TTokenType.Plus, TTokenType.Minus]) then
  begin
    b := (TokenType = TTokenType.Minus);
    GetToken();
  end;
  Result := Level6();
  If b then Result := new TFPNegateOperation(Result);
end;

method TFPExpressionParser.Level6: TFPExprNode;
begin
  {$ifdef debugexpr}  Writeln('Level 6 ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  if (TokenType = TTokenType.Left) then
  begin
    GetToken();
    Result := Level1();
    try
      if (TokenType <> TTokenType.Right) then ParserError(String.Format(ErrorStrings.SErrBracketExpected, Scanner.Pos, CurrentToken));
      GetToken();
    Except
      disposeAndNil(Result);
      raise;
    end;
  end
  else
    result := Primitive();
end;

method TFPExpressionParser.Primitive: TFPExprNode;
Var
  I : Int64;
  X : Double;
  ACount : Integer;
  IFF : Boolean;
  IFC : Boolean;
  ID : TFPExprIdentifierDef;
  Args : TExprArgumentArray;
  AI : Integer;
begin
  {$ifdef debugexpr}  Writeln('Primitive : ',TokenName(TokenType),': ',CurrentToken);{$endif debugexpr}
  HelperFunctions.SetLength(var Args, 0);
  if (TokenType = TTokenType.Number) then
  begin
    if Int64.TryParse(CurrentToken, out I) then
      Result := new TFPConstExpression(I)
    else
    begin
      if Double.TryParse(CurrentToken, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out X) then result := new TFPConstExpression(X)
      else ParserError(String.Format(ErrorStrings.SErrInvalidFloat, CurrentToken));
    end;
  end
  else if (TokenType=TTokenType.String) then
    Result := new TFPConstExpression(CurrentToken)
    else if (TokenType in [TTokenType.True, TTokenType.False]) then
  Result := new TFPConstExpression(Boolean(TokenType = TTokenType.True))
    else if Not (TokenType in [TTokenType.Identifier, TTokenType.if, TTokenType.Case]) then
        ParserError(String.Format(ErrorStrings.SerrUnknownTokenAtPos, Scanner.Pos, CurrentToken))
      else
      begin
      IFF := TokenType = TTokenType.if;
      IFC := TokenType = TTokenType.Case;
      if not (IFF or IFC) then
      begin
        ID := self.IdentifierByName(CurrentToken);
        If (ID = Nil) then ParserError(String.Format(ErrorStrings.SErrUnknownIdentifier,[CurrentToken]));
      end;
      // Determine number of arguments
      if IFF then
      ACount := 3
      else if IFC then
        ACount := -4
      else if (ID.IdentifierType in [TIdentifierType.Function]) then
        ACount := ID.ArgumentCount
      else
        ACount := 0;
      // Parse arguments.
      // Negative is for variable number of arguments, where Abs(value) is the minimum number of arguments
      If (ACount <> 0) then
      begin
        GetToken();
        If (TokenType <> TTokenType.Left) then ParserError(String.Format(ErrorStrings.SErrLeftBracketExpected, Scanner.Pos, CurrentToken));
        HelperFunctions.SetLength(var Args, Math.Abs(ACount));
        AI := 0;
        try
          repeat
            GetToken();
            // Check if we must enlarge the argument array
            If (ACount < 0) and (AI = length(Args)) then
            begin
              HelperFunctions.SetLength(var Args, AI + 1);
              Args[AI] := nil;
            end;
            Args[AI] := Level1();
            inc(AI);
            //If (TokenType <> TTokenType.ttComma) or (TokenType <> TTokenType.Right) then If (AI < Math.Abs(ACount)) then ParserError(String.Format(ErrorStrings.SErrCommaExpected, Scanner.Pos, CurrentToken));
          until (AI = ACount) or (TokenType = TTokenType.Right);
          If TokenType <> TTokenType.Right then ParserError(String.Format(ErrorStrings.SErrBracketExpected, Scanner.Pos, CurrentToken));
        except
          on E: Exception do
          begin
            dec(AI);
            While (AI >= 0) do
            begin
              disposeAndNil(Args[AI]);
              dec(AI);
            end;
            raise;
          end;
        end;
      end;
      If IFF then
      Result := new TIfOperation(Args[0], Args[1], Args[2])
      else If IFC then
      Result := new TCaseOperation(Args)
        else
          Case ID.IdentifierType of
            TIdentifierType.Variable:
            begin
              var temp := new TFPExprVariable(ID);
              result := temp;
              UsedVariablesInFormula.Add(temp);
            end;
            TIdentifierType.Function:
            begin
              result := new TFPFunctionCallBack(ID, Args);
            end;
          end;
    end;
  GetToken;
end;

method TFPExpressionParser.SetExpression(const aValue: String);
begin
  UsedVariablesInFormula.Clear();
  fExpression := aValue;
  fScanner.Source := aValue;
  If assigned(fExprNode) then disposeAndNil(fExprNode);
  If not String.IsNullOrEmpty(fExpression) then
  begin
    GetToken();
    fExprNode := Level1();
    If (TokenType <> TTokenType.EOF) then ParserError(String.Format(ErrorStrings.SErrUnterminatedExpression, Scanner.Pos, CurrentToken));
    fExprNode.Check;
  end
  else
    fExprNode := Nil;
end;

method TFPExpressionParser.CheckResultType(const Res: TFPExpressionResult; AType: TResultType);
begin
  If (Res.ResultType <> AType) then HelperFunctions.RaiseParserError(ErrorStrings.SErrInvalidResultType,[HelperFunctions.ResultTypeName(Res.ResultType)]);
end;

method TFPExpressionParser.Evaluate: TFPExpressionResult;
begin
  EvaluateExpression(var result);
end;

method TFPExpressionParser.ResultType: TResultType;
begin
  if not assigned(fExprNode) then ParserError(ErrorStrings.SErrInExpression);
  Result := fExprNode.NodeType;
end;

method TFPExpressionParser.IdentifiersCollectionChanged(sender: Object; e: System.Collections.Specialized.NotifyCollectionChangedEventArgs);
begin
  fDirty := true;
end;

method TFPExpressionParser.GetAllFunctions: List<IFormulaFunction>;
begin
  result := new List<IFormulaFunction>;
  for each fn in &Functions do
  begin
    result.Add(fn.Function);
  end;
end;

method TFPExpressionParser.SetVariables(value: TFPExprIdentifierDefs);
begin
  if assigned(fVariables) then fVariables.CollectionChanged -= IdentifiersCollectionChanged;
  fVariables := value;
  fVariables.CollectionChanged += IdentifiersCollectionChanged;
end;

method TFPExpressionParser.setFunctions(value: TFPExprIdentifierDefs);
begin
  if assigned(fFunctions) then fFunctions.CollectionChanged -= IdentifiersCollectionChanged;
  fFunctions := value;
  fFunctions.CollectionChanged += IdentifiersCollectionChanged;
end;

{ ---------------------------------------------------------------------
TFPExprIdentifierDefs
---------------------------------------------------------------------}

method TFPExprIdentifierDefs.GetI(AIndex : Integer): TFPExprIdentifierDef;
begin
  Result := TFPExprIdentifierDef(Item[AIndex]);
end;

method TFPExprIdentifierDefs.SetI(AIndex : Integer; const AValue: TFPExprIdentifierDef);
begin
  Item[AIndex] := AValue;
end;

method TFPExprIdentifierDefs.IndexOfIdentifier(const AName: String): Integer;
begin
  Result := Count-1;
  While (Result >= 0) And (String.Compare(GetI(Result).Name, AName) <> 0) do dec(Result);
end;

method TFPExprIdentifierDefs.FindIdentifier(const AName: String): TFPExprIdentifierDef;
Var
  I : Integer;
begin
  I:=IndexOfIdentifier(AName);
  If (I=-1) then
    Result := Nil
  else
    Result := GetI(I);
end;

method TFPExprIdentifierDefs.IdentifierByName(const AName: String): TFPExprIdentifierDef;
begin
  Result:=FindIdentifier(AName);
  if (Result=Nil) then HelperFunctions.RaiseParserError(ErrorStrings.SErrUnknownIdentifier,AName);
end;

method TFPExprIdentifierDefs.AddVariable(aVariable: FormulaVariable): TFPExprIdentifierDef;
begin
  Result := new TFPExprIdentifierDef;
  Result.IdentifierType := TIdentifierType.Variable;
  Result.Variable := aVariable;
  Add(Result);
end;

method TFPExprIdentifierDefs.AddVariableRange(aVariableRange: IEnumerable<FormulaVariable>): List<TFPExprIdentifierDef>;
begin
  Result := new List<TFPExprIdentifierDef>();
  for each fv in aVariableRange do
  begin
    var TFPExprIdCurrent := new TFPExprIdentifierDef();
    TFPExprIdCurrent.IdentifierType := TIdentifierType.Variable;
    TFPExprIdCurrent.Variable := fv;
    self.Items.Add(TFPExprIdCurrent);
  end;

  self.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs('Count'));
  self.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs('Item[]'));
  self.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
end;

method TFPExprIdentifierDefs.AddFunction(aFunctionObject: IFormulaFunction): TFPExprIdentifierDef;
begin
  Result := new TFPExprIdentifierDef;
  Result.IdentifierType := TIdentifierType.Function;
  result.ArgumentCount := aFunctionObject.MaxNumberOfArgs;
  Result.Function := aFunctionObject;
  Add(Result);
end;

method TFPExprIdentifierDefs.AddFunctionRange(aFunctionObjectRange: IEnumerable<IFormulaFunction>): List<TFPExprIdentifierDef>;
begin
  Result := new List<TFPExprIdentifierDef>();
  for each fo in aFunctionObjectRange do
  begin
    var TFPExprIdCurrent := new TFPExprIdentifierDef();
    TFPExprIdCurrent.IdentifierType := TIdentifierType.Function;
    TFPExprIdCurrent.ArgumentCount := fo.MaxNumberOfArgs;
    TFPExprIdCurrent.Function := fo;
    self.Items.Add(TFPExprIdCurrent);
  end;

  self.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs('Count'));
  self.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs('Item[]'));
  self.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
end;

{ ---------------------------------------------------------------------
TFPExprIdentifierDef
---------------------------------------------------------------------}

method TFPExprIdentifierDef.CheckResultType(const AType: TResultType);
begin
  If ResultType <> AType then HelperFunctions.RaiseParserError(ErrorStrings.SErrInvalidResultType,[HelperFunctions.ResultTypeName(AType)])
end;

method TFPExprIdentifierDef.CheckVariable;
begin
  If IdentifierType <> TIdentifierType.Variable then HelperFunctions.RaiseParserError(ErrorStrings.SErrNotVariable,[Name]);
end;

method TFPExprIdentifierDef.GetName: String;
begin
  case IdentifierType of
    TIdentifierType.Variable: exit Variable.Name;
    TIdentifierType.Function: exit &Function.Name;
  end;
end;

method TFPExprIdentifierDef.GetResultType: TResultType;
begin
  case IdentifierType of
    TIdentifierType.Variable: exit Variable.Value.ResultType;
    TIdentifierType.Function: exit &Function.ResultType;
  end;
end;

{ TFPBinaryOperation }

method TFPBinaryOperation.CheckSameNodeTypes;
Var
  LT,RT : TResultType;
begin
  LT:=left.NodeType;
  RT:=Right.NodeType;
  if (RT<>LT) then HelperFunctions.RaiseParserError(ErrorStrings.SErrTypesDoNotMatch,[HelperFunctions.ResultTypeName(LT), HelperFunctions.ResultTypeName(RT),left.ToString,Right.ToString])
end;

constructor TFPBinaryOperation(ALeft, ARight: TFPExprNode);
begin
  FLeft := ALeft;
  FRight := ARight;
end;

finalizer TFPBinaryOperation;
begin
  disposeAndNil(FLeft);
  disposeAndNil(FRight);
end;

method TFPBinaryOperation.Check;
begin
  If not assigned(left) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoleftOperand, typeOf(self));
  If not assigned(Right) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoRightOperand, typeOf(self));
  left.UsedInBinaryOperation := true;
  Right.UsedInBinaryOperation := true;
end;

{ TFPUnaryOperator }

constructor TFPUnaryOperator(AOperand: TFPExprNode);
begin
  FOperand := AOperand;
end;

finalizer TFPUnaryOperator;
begin
  disposeAndNil(FOperand);
end;

method TFPUnaryOperator.Check;
begin
  If Not assigned(Operand) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoOperand,typeOf(self));
end;

{ TFPConstExpression }

constructor TFPConstExpression(AValue: String);
begin
  FValue.ResultType := TResultType.String;
  FValue.ResString:=AValue;
end;

constructor TFPConstExpression(AValue: Int64);
begin
  FValue.ResultType := TResultType.Float;
  FValue.ResFloat := AValue;
end;

constructor TFPConstExpression(AValue: Double);
begin
  Inherited constructor;
  FValue.ResultType := TResultType.Float;
  FValue.ResFloat := AValue;
end;

constructor TFPConstExpression(AValue: Boolean);
begin
  FValue.ResultType := TResultType.Boolean;
  FValue.ResBoolean := AValue;
end;

method TFPConstExpression.Check;
begin
  // Nothing to check;
end;

method TFPConstExpression.NodeType: TResultType;
begin
  Result:=FValue.ResultType;
end;

method TFPConstExpression.GetNodeValue(var aResult : TFPExpressionResult);
begin
  aResult := FValue;
end;

method TFPConstExpression.ToString: String ;
begin
  Case NodeType of
    TResultType.String  : Result := '''' + FValue.ResString + '''';
    TResultType.Boolean : If FValue.ResBoolean then Result := 'True' else Result := 'False';
    TResultType.Float   : Result := FValue.ResFloat.ToString();
  end;
end;

{ TFPNegateOperation }
method TFPNegateOperation.Check;
begin
  Inherited;
  If Not (Operand.NodeType in [TResultType.Float]) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoNegation,[HelperFunctions.ResultTypeName(Operand.NodeType),Operand.ToString])
end;

method TFPNegateOperation.NodeType: TResultType;
begin
  Result:=Operand.NodeType;
end;

method TFPNegateOperation.GetNodeValue(var aResult : TFPExpressionResult);
begin
  Operand.GetNodeValue(var aResult);
  Case aResult.ResultType of
    TResultType.Float : aResult.ResFloat := - aResult.ResFloat;
  end;
end;

method TFPNegateOperation.ToString: String;
begin
  Result := '-' + Operand.ToString().TrimStart();
end;

{ TFPBinaryAndOperation }
method TFPBooleanOperation.Check;
begin
  inherited Check;
  CheckNodeType(left,[TResultType.Boolean]);
  CheckNodeType(Right,[TResultType.Boolean]);
  CheckSameNodeTypes;
end;

method TFPBooleanOperation.NodeType: TResultType;
begin
  Result := left.NodeType;
end;

method TFPBinaryAndOperation.GetNodeValue(var aResult: TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Boolean : aResult.ResBoolean:=aResult.ResBoolean and RRes.ResBoolean;
  end
end;

method TFPBinaryAndOperation.ToString: String;
begin
  Result := left.ToString + ' and ' + Right.ToString;
end;

{ TFPExprNode }
method TFPExprNode.CheckNodeType(aNode: TFPExprNode; Allowed: TResultTypes);
begin
  if not assigned(aNode) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoNodeToCheck);
  if not (aNode.NodeType in Allowed) then
  begin
    var s := String.Empty;
    for a: TResultType := low(TResultType) to high(TResultType) do
      if a in Allowed then
      begin
        if not String.IsNullOrEmpty(s) then s := s + ',';
        s := s + HelperFunctions.ResultTypeName(a);
      end;
    HelperFunctions.RaiseParserError(ErrorStrings.SInvalidNodeType, [HelperFunctions.ResultTypeName(aNode.NodeType), s, aNode.ToString]);
  end;
end;

method TFPExprNode.NodeValue: TFPExpressionResult;
begin
  GetNodeValue(var result);
end;

constructor TFPExprNode;
begin

end;

{ TFPBinaryOrOperation }
method TFPBinaryOrOperation.ToString: String;
begin
  Result := left.ToString + ' or ' + Right.ToString;
end;

method TFPBinaryOrOperation.GetNodeValue(var aResult: TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Boolean : aResult.ResBoolean:=aResult.ResBoolean or RRes.ResBoolean;
  end;
end;

{ TFPBinaryXOrOperation }

method TFPBinaryXOrOperation.ToString: String;
begin
  Result := left.ToString+' xor '+Right.ToString;
end;

method TFPBinaryXOrOperation.GetNodeValue(var aResult : TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Boolean : aResult.ResBoolean:=aResult.ResBoolean xor RRes.ResBoolean;
  end;
end;

{ TFPNotNode }
method TFPNotNode.Check;
begin
  if not (Operand.NodeType in [TResultType.Boolean]) then HelperFunctions.RaiseParserError(ErrorStrings.SErrNoNOTOperation,[HelperFunctions.ResultTypeName(Operand.NodeType),Operand.ToString])
end;

method TFPNotNode.NodeType: TResultType;
begin
  Result := Operand.NodeType;
end;

method TFPNotNode.GetNodeValue(var aResult: TFPExpressionResult);
begin
  Operand.GetNodeValue(var aResult);
  Case aResult.ResultType of
    TResultType.Boolean : aResult.ResBoolean := Not aResult.ResBoolean;
  end
end;

method TFPNotNode.ToString: String;
begin
  Result := 'not ' + Operand.ToString;
end;

{ TIfOperation }
constructor TIfOperation(ACondition, ALeft, ARight: TFPExprNode);
begin
  Inherited constructor(ALeft,ARight);
  fCondition := ACondition;
end;

finalizer TIfOperation;
begin
  disposeAndNil(fCondition);
end;

method TIfOperation.GetNodeValue(var aResult: TFPExpressionResult);
begin
  fCondition.GetNodeValue(var aResult);
  If aResult.ResBoolean then
    Left.GetNodeValue(var aResult)
  else
    Right.GetNodeValue(var aResult)
end;

method TIfOperation.Check;
begin
  inherited Check;
  if (Condition.NodeType <> TResultType.Boolean) then HelperFunctions.RaiseParserError(ErrorStrings.SErrIFNeedsBoolean,[Condition.ToString]);
  CheckSameNodeTypes;
end;

method TIfOperation.NodeType: TResultType;
begin
  Result := Left.NodeType;
end;

method TIfOperation.ToString: String;
begin
  Result := String.Format('if({0} , {1} , {2})', Condition.ToString, Left.ToString, Right.ToString);
end;

{ TCaseOperation }

method TCaseOperation.GetNodeValue(var aResult: TFPExpressionResult);
var
  I,L : Integer;
  B : Boolean;
  RT,RV : TFPExpressionResult;
begin
  fArgs[0].GetNodeValue(var RT);
  L := length(fArgs);
  I := 2;
  B := false;
  while (not B) and (I<L) do
  begin
    fArgs[I].GetNodeValue(var RV);
    Case RT.ResultType of
      TResultType.Boolean  : B := (RT.ResBoolean = RV.ResBoolean);
      TResultType.Float    : B := (RT.ResFloat = RV.ResFloat);
      TResultType.String   : B := (RT.ResString = RV.ResString);
    end;
    if not B then inc(I, 2);
  end;
  // Set aResult type.
  aResult.ResultType := fArgs[1].NodeType;
  if B then
    fArgs[I+1].GetNodeValue(var aResult)
  else if ((L mod 2) = 0) then
    fArgs[1].GetNodeValue(var aResult);
end;

method TCaseOperation.Check;
Var
  T,V : TResultType;
  N : TFPExprNode;
begin
  If (length(fArgs)<3) then HelperFunctions.RaiseParserError(ErrorStrings.SErrCaseNeeds3);
  If ((length(fArgs) mod 2)=1) then HelperFunctions.RaiseParserError(ErrorStrings.SErrCaseEvenCount);
  T := fArgs[0].NodeType;
  V := fArgs[1].NodeType;
  for i: Integer := 2 to length(fArgs)-1 do
  begin
    N := fArgs[i];
    // Even argument types (labels) must equal tag.
    if ((i mod 2) = 0) then
    begin
      if not (N is TFPConstExpression) then HelperFunctions.RaiseParserError(ErrorStrings.SErrCaseLabelNotAConst,[i div 2, N.ToString]);
      if (N.NodeType <> T) then HelperFunctions.RaiseParserError(ErrorStrings.SErrCaseLabelType,[i div 2, N.ToString,HelperFunctions.ResultTypeName(T),HelperFunctions.ResultTypeName(N.NodeType)]);
    end
    else // Odd argument types (values) must match first.
    begin
      if (N.NodeType<>V) then HelperFunctions.RaiseParserError(ErrorStrings.SErrCaseValueType,[(i-1)div 2,N.ToString,HelperFunctions.ResultTypeName(V),HelperFunctions.ResultTypeName(N.NodeType)]);
    end
  end;
end;

method TCaseOperation.NodeType: TResultType;
begin
  Result:=fArgs[1].NodeType;
end;

constructor TCaseOperation(Args: TExprArgumentArray);
begin
  fArgs := Args;
end;

finalizer TCaseOperation;
begin
  for i: Integer := 0 to length(fArgs)-1 do disposeAndNil(fArgs[i]);
end;

method TCaseOperation.ToString: String;
begin
  Result:='';
  for i: Integer := 0 to fArgs.Length - 1 do
  begin
    If not String.IsNullOrEmpty(Result) then Result := Result + ', ';
    Result := Result + fArgs[i].ToString;
  end;
  Result:='Case(' + Result + ')';
end;

{ TFPBooleanaResultOperation }
method TFPBooleanResultOperation.Check;
begin
  inherited Check;
  CheckSameNodeTypes;
end;

method TFPBooleanResultOperation.NodeType: TResultType;
begin
  Result:=TResultType.Boolean;
end;

{ TFPEqualOperation }
method TFPEqualOperation.ToString: String;
begin
  Result := Left.ToString + ' = ' + Right.ToString;
end;

method TFPEqualOperation.GetNodeValue(var aResult: TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Boolean  : aResult.ResBoolean := aResult.ResBoolean=RRes.ResBoolean;
    TResultType.Float    : aResult.ResBoolean := aResult.ResFloat=RRes.ResFloat;
    TResultType.String   : aResult.ResBoolean := aResult.ResString=RRes.ResString;
  end;
  aResult.ResultType:= TResultType.Boolean;
end;

{ TFPUnequalOperation }

method TFPUnequalOperation.ToString: String;
begin
  Result := Left.ToString + ' <> ' + Right.ToString;
end;

method TFPUnequalOperation.GetNodeValue(var aResult : TFPExpressionResult);
begin
  Inherited GetNodeValue(var aResult);
  aResult.ResBoolean := Not aResult.ResBoolean;
end;


{ TFPLessThanOperation }

method TFPLessThanOperation.ToString: String;
begin
  Result := Left.ToString + ' < ' + Right.ToString;
end;

method TFPLessThanOperation.GetNodeValue(var aResult : TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Float    : aResult.ResBoolean:=aResult.ResFloat<RRes.ResFloat;
    TResultType.String   : aResult.ResBoolean:=aResult.ResString<RRes.ResString;
  end;
  aResult.ResultType:=TResultType.Boolean;
end;

{ TFPGreaterThanOperation }

method TFPGreaterThanOperation.ToString: String;
begin
  Result := Left.ToString + ' > ' + Right.ToString;
end;

method TFPGreaterThanOperation.GetNodeValue(var aResult: TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  Case aResult.ResultType of
    TResultType.Float   : aResult.ResBoolean := aResult.ResFloat > RRes.ResFloat;
    TResultType.String  : aResult.ResBoolean := aResult.ResString > RRes.ResString;
  end;
  aResult.ResultType:=TResultType.Boolean;
end;

{ TFPGreaterThanEqualOperation }

method TFPGreaterThanEqualOperation.ToString: String;
begin
  Result := Left.ToString + ' >= ' + Right.ToString;
end;

method TFPGreaterThanEqualOperation.GetNodeValue(var aResult : TFPExpressionResult);
begin
  Inherited GetNodeValue(var aResult);
  aResult.ResBoolean := Not aResult.ResBoolean;
end;

{ TFPLessThanEqualOperation }

method TFPLessThanEqualOperation.ToString: String;
begin
  Result := Left.ToString + ' <= ' + Right.ToString;
end;

method TFPLessThanEqualOperation.GetNodeValue(var aResult: TFPExpressionResult);
begin
  Inherited GetNodeValue(var aResult);
  aResult.ResBoolean := not aResult.ResBoolean;
end;

{ TFPOrderingOperation }
method TFPOrderingOperation.Check;
var
  AllowedTypes: TResultTypes := [TResultType.Float, TResultType.String];
begin
  CheckNodeType(Left, AllowedTypes);
  CheckNodeType(Right, AllowedTypes);
  inherited Check;
end;

{ TMathOperation }

method TMathOperation.Check;
var
  AllowedTypes: TResultTypes := [TResultType.Float, TResultType.String];
begin
  inherited Check;
  CheckNodeType(Left, AllowedTypes);
  CheckNodeType(Right, AllowedTypes);
  CheckSameNodeTypes;
end;

method TMathOperation.NodeType: TResultType;
begin
  Result := Left.NodeType;
end;

{ TFPAddOperation }

method TFPAddOperation.ToString: String;
begin
  Result := Left.ToString + ' + ' + Right.ToString;
end;

method TFPAddOperation.GetNodeValue(var aResult : TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  case aResult.ResultType of
    TResultType.String   : aResult.ResString := aResult.ResString + RRes.ResString;
    TResultType.Float    : aResult.ResFloat := aResult.ResFloat + RRes.ResFloat;
  end;
  aResult.ResultType := NodeType;
end;

{ TFPSubtractOperation }

method TFPSubtractOperation.Check;
var
  AllowedTypes: TResultTypes := [TResultType.Float];
begin
  CheckNodeType(Left, AllowedTypes);
  CheckNodeType(Right, AllowedTypes);
  inherited Check;
end;

method TFPSubtractOperation.ToString: String;
begin
  Result := Left.ToString + ' - ' + Right.ToString;
end;

method TFPSubtractOperation.GetNodeValue(var aResult : TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  case aResult.ResultType of
    TResultType.Float    : aResult.ResFloat:=aResult.ResFloat-RRes.ResFloat;
  end;
end;

{ TFPMultiplyOperation }
method TFPMultiplyOperation.Check;
var
  AllowedTypes: TResultTypes := [TResultType.Float];
begin
  CheckNodeType(Left, AllowedTypes);
  CheckNodeType(Right, AllowedTypes);
  Inherited;
end;

method TFPMultiplyOperation.ToString: String;
begin
  result := Left.ToString + ' * ' + Right.ToString;
end;

method TFPMultiplyOperation.GetNodeValue(var aResult : TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  Left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  case aResult.ResultType of
    TResultType.Float    : aResult.ResFloat := aResult.ResFloat * RRes.ResFloat;
  end;
end;

{ TFPDivideOperation }

method TFPDivideOperation.Check;
var
  AllowedTypes: TResultTypes := [TResultType.Float];
begin
  CheckNodeType(Left,AllowedTypes);
  CheckNodeType(Right,AllowedTypes);
  inherited Check;
end;

method TFPDivideOperation.ToString: String;
begin
  Result := Left.ToString + ' / ' + Right.ToString;
end;

method TFPDivideOperation.NodeType: TResultType;
begin
  Result := TResultType.Float;
end;

method TFPDivideOperation.GetNodeValue(var aResult: TFPExpressionResult);
Var
  RRes : TFPExpressionResult;
begin
  left.GetNodeValue(var aResult);
  Right.GetNodeValue(var RRes);
  case aResult.ResultType of
    TResultType.Float    : aResult.ResFloat:=aResult.ResFloat / RRes.ResFloat;
  end;
  aResult.ResultType := TResultType.Float;
end;

{ TFPConvertNode }

method TFPConvertNode.ToString: String;
begin
  Result := Operand.ToString;
end;

{ TFPExprIdentifierNode }

constructor TFPExprIdentifierNode(aID: TFPExprIdentifierDef);
begin
  fID := aID;
  FResultType := fID.ResultType;
end;

method TFPExprIdentifierNode.NodeType: TResultType;
begin
  Result := FResultType;
end;

{ TFPExprVariable }
method TFPExprVariable.Check;
begin
  // Do nothing;
end;

method TFPExprVariable.ToString: String;
begin
  Result := fID.Name;
end;

method TFPExprVariable.GetNodeValue(var aResult: TFPExpressionResult);
begin
  aResult := fID.Variable.Value;
end;

{ TFPExprFunction }
method TFPExprFunction.CalcParams;
begin
  for i: Integer := 0 to FargumentParams.Length - 1 do
  begin
    var argumentNode := FArgumentNodes[i];
    if assigned(argumentNode) then argumentNode.GetNodeValue(var FargumentParams[i]);
  end;
end;

method TFPExprFunction.Check;
begin
  If length(FArgumentNodes) <> fID.ArgumentCount then 
  begin
    HelperFunctions.RaiseParserError(ErrorStrings.ErrInvalidArgumentCount,[fID.Name]);
  end;
  For i: Integer := 0 to length(FArgumentNodes)-1 do
  begin
    if not assigned(FArgumentNodes[i]) then continue;
    var rta := FArgumentNodes[i].NodeType;
    if not fID.Function.MultArgsSupported(i, rta) then 
    begin
      HelperFunctions.RaiseParserError(ErrorStrings.SErrInvalidArgumentType, [i+1, HelperFunctions.ResultTypeName(fID.Function.GetExpectedArgType(i)) ,HelperFunctions.ResultTypeName(rta)])
    end;
  end;
end;

constructor TFPExprFunction(AID: TFPExprIdentifierDef; const Args: TExprArgumentArray);
begin
  Inherited constructor(AID);
  FArgumentNodes := Args;
  HelperFunctions.SetLength(var FargumentParams, length(Args));
end;

finalizer TFPExprFunction;
begin
  For each i in FArgumentNodes do disposeAndNil(i);
end;

method TFPExprFunction.ToString: String;
begin
  var s := String.Empty;
  for each argNode in FArgumentNodes do
  begin
    if not assigned(argNode) then continue;
    if (s <> '') then s := s + ',';
    s := s + argNode.ToString;
  end;
  if (s <> '') then s := '(' + s + ')';
  result := fID.Name + s;
end;

{ TFPFunctionCallBack }

constructor TFPFunctionCallBack(AID: TFPExprIdentifierDef; Const Args : TExprArgumentArray);
begin
  Inherited;
  CallBack := AID.Function;
end;

method TFPFunctionCallBack.GetNodeValue(var aResult: TFPExpressionResult);
begin
  If FargumentParams.Length > 0 then CalcParams;
  CallBack.Evaluate(var aResult, FargumentParams);
  aResult.ResultType := NodeType;
end;

constructor ExpressionScannerException(aMessage: String; aInvalidToken: String; aInvalidCharacterPosition: Integer);
begin
  inherited constructor(aMessage);
  self.InvalidToken := aInvalidToken;
  self.InvalidCharacterPosition := aInvalidCharacterPosition;
end;

constructor ExpressionScannerException;
begin

end;

method TFPPowerOperation.Check;
begin
  var AllowedTypes: TResultTypes := [TResultType.Float];
  CheckNodeType(left, AllowedTypes);
  CheckNodeType(Right, AllowedTypes);
  Inherited;
end;

method TFPPowerOperation.ToString: String;
begin
  result := left.ToString + ' ^ ' + Right.ToString;
end;

method TFPPowerOperation.GetNodeValue(var aResult: TFPExpressionResult);
begin
  var rres: TFPExpressionResult;
  left.GetNodeValue(var aResult);
  Right.GetNodeValue(var rres);
  case aResult.ResultType of
    TResultType.Float: aResult.ResFloat := Math.Pow(aResult.ResFloat, rres.ResFloat);
  end;
end;

end.