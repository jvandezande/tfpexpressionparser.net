﻿{
    This file was part of the Free Component Library (FCL)
    Copyright (c) 2008 Michael Van Canneyt.

    Modified for Oxygene by Jeroen Vandezande Copyright (c) 2016

    Expression parser, supports variables, functions and
    float/string/boolean operations.

    The source code of the Free Pascal Runtime Libraries and packages are
    distributed under the Library GNU General Public License
    with the following modification:

    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the terms
    and conditions of the license of that module. An independent module is a module
    which is not derived from or based on this library. If you modify this
    library, you may extend this exception to your version of the library, but you are
    not obligated to do so. If you do not wish to do so, delete this exception
    statement from your version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 }
namespace fpexprpars;

interface

uses
  System.Collections.Generic,
  System.Linq,
  System.Text;

type

  HelperFunctions = public static class
  private
  protected
  assembly
    method RaiseParserError(Msg: String);
    method RaiseParserError(Fmt: String; params Args: array of Object);
    method TokenName(aToken: TTokenType): String;
    method ResultTypeName(aResult: TResultType): String;
    method SetLength<T>(var aArray: array of T; aLength: Integer);
  end;

implementation

method HelperFunctions.SetLength<T>(var aArray: array of T; aLength: Integer);
  begin
    if aLength > 0 then
    begin
      var temp := new T[aLength];
      aArray.CopyTo(temp, 0);
      aArray := temp;
    end
    else
    begin
      aArray := new T[0];
    end;
  end;

  method HelperFunctions.RaiseParserError(Msg: String);
  begin
    raise new ExpressionParserException(Msg);
  end;

  method HelperFunctions.RaiseParserError(Fmt: String; params Args: array of Object);
  begin
    Raise new ExpressionParserException(String.Format(Fmt, Args));
  end;

  method HelperFunctions.TokenName(aToken: TTokenType): String;
  begin
    Result := aToken.ToString();
  end;

  method HelperFunctions.ResultTypeName(aResult: TResultType): String;
  begin
    Result := aResult.ToString();
  end;

end.