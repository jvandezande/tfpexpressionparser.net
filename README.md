# TFPExpressionParser.NET  #

This is a fork of the TFPExpressionParser that is included in the Free Pascal Compiler Project: [TFPExpressionParser](http://wiki.freepascal.org/How_To_Use_TFPExpressionParser)

Original Code: [Code on GitHub](https://github.com/graemeg/freepascal/blob/master/packages/fcl-base/src/fpexprpars.pp)

I modified the excellent code of Michael Van Canneyt to compile in [Oxygene](http://www.elementscompiler.com/elements/oxygene/).

I made some modifications to simplify the library and make it more in line with the .NET guidelines. (e.g. TDateTime functions are dropped and there is no difference between Integer and Floating point calculations)

The code is released under the same license as the original code:


```
    Copyright (c) 2008 Michael Van Canneyt.

    Expression parser, supports variables, functions and
    float/integer/string/boolean/datetime operations.

    The source code of the Free Pascal Runtime Libraries and packages are 
    distributed under the Library GNU General Public License 
    (see the file COPYING) with the following modification:

    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the terms
    and conditions of the license of that module. An independent module is a module
    which is not derived from or based on this library. If you modify this
    library, you may extend this exception to your version of the library, but you are
    not obligated to do so. If you do not wish to do so, delete this exception
    statement from your version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


```