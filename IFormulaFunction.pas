﻿{
    This file was part of the Free Component Library (FCL)
    Copyright (c) 2008 Michael Van Canneyt.

    Modified for Oxygene by Jeroen Vandezande Copyright (c) 2016

    Expression parser, supports variables, functions and
    float/string/boolean operations.

    The source code of the Free Pascal Runtime Libraries and packages are 
    distributed under the Library GNU General Public License 
    with the following modification:

    As a special exception, the copyright holders of this library give you
    permission to link this library with independent modules to produce an
    executable, regardless of the license terms of these independent modules,
    and to copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the terms
    and conditions of the license of that module. An independent module is a module
    which is not derived from or based on this library. If you modify this
    library, you may extend this exception to your version of the library, but you are
    not obligated to do so. If you do not wish to do so, delete this exception
    statement from your version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 }
namespace fpexprpars;

interface

type

  IFormulaFunction = public interface
    method Evaluate(var aResult: TFPExpressionResult; Args: TExprParameterArray);
    /// <summary>
    /// This method will verify if a argument at location is valid
    /// </summary>
    /// <param name="Count">The location of the parameter (0 is first param)</param>
    /// <param name="aArgType">The type of the param</param>
    /// <returns>true if the parameter is valid</returns>
    method MultArgsSupported(Count: Int32; aArgType: TResultType): Boolean;
    method GetExpectedArgType(aArgumentIndex: Int32): TResultType;
    property Description: String read;
    property Name: String read;
    property ResultType: TResultType read;
    property MaxNumberOfArgs: Integer read;
  end;
  
implementation

end.
